// Evol scripts.
// Authors:
//    Qwerty Dragon
//    Reid
//    WildX
// Description:
//    A small note presenting the 7 main rules of Evol Online.

001-2-28,36,28,0	script	Note#001-2-28	NPC_PAPER_NOTE,{
    narrator
        l("There is a paper with some rules written on it."),
        l("1. ##BDo not bot##b, this means you are not allowed to perform any AFK (away from keyboard) activity, apart from standing idle."),
        l("2. ##BDo not use offensive/rude language##b in the chats or in your character(s) name(s)."),
        l("3. ##BDo not spam/flood other players.##b This includes chat spam and spam by trade requests."),
        l("4. ##BSpeak only English in public areas.##b You can speak whatever language you want through whispers or whenever everyone in the area can speak said language."),
        l("5. ##BDo not beg others##b for money, items or favours of any kind. If you want to ask for something, do it politely and only once. Try not to annoy other players."),
        l("6. ##BFollow the [@@http://wiki.evolonline.org/rules/esc|ESC@@]##b (Evol Social Convention)."),
        l("7. ##BDo not multibox.##b You are not allowed to engage in combat while controlling more than one character at a time."),
        l("Following these lines are some other writings on this paper."),
        l("Do not give the password of your room to anybody! Keep it secret and try not to use the same one in any other room in the future. - Jenna");

    close;

OnInit:
    .distance = 2;
    end;
}
