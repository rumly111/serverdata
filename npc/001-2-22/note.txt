// Evol scripts.
// Authors:
//    Vasily_Makarov
// Description:
//    

001-2-1.gat,22,33,0	script	Note	NPC_NO_SPRITE,{
    mesn;
    mesq l("This note was left by somebody.");
    next;
    mesq l("What do you want to do with it?");

    menu
        l("Read it"), L_Content,
        l("Leave it"), -;
    close;

L_Content:
    mesn;
    mes "\"" + l("Dear sister,");
    mes l("On the next days we will finally arrives at Artis");
    next;

    mes l("I will send you this letter directly when I arrive");
    mes l("Don't worry sister, I didn't forget you");
    next;

    mes l("I hope to come back home when the days are better");
    mes l("And when we have enough money to support ourselves and not depend on anybody");

    mes l("Sincerely yours, Dan") + "\"";
    close;
}
